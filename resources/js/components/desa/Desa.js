import React, { Component } from "react";
import axios from "axios";
import { FaPlus } from "react-icons/fa";
import { Row, Col, Card, Button, Alert } from "react-bootstrap";

import { DesaModal, DesaDeleteModal } from "./DesaModal";
import DesaTable from "./DesaTable";

export default class Desa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            desaData: [],
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                kecamatan_id: "",
                kepala_desa: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateDesaId: null,
            deleteDesaId: null,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        };

        this.handleShowInsertModal = this.handleShowInsertModal.bind(this);
        this.handleShowUpdateModal = this.handleShowUpdateModal.bind(this);
        this.handleShowDeleteModal = this.handleShowDeleteModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        this.handleNamaChange = this.handleNamaChange.bind(this);
        this.handleKecamatanChange = this.handleKecamatanChange.bind(this);
        this.handleKepalaDesaChange = this.handleKepalaDesaChange.bind(this);
        this.handleLuasChange = this.handleLuasChange.bind(this);
        this.handleSatuanLuasChange = this.handleSatuanLuasChange.bind(this);
        this.handlePopulasiChange = this.handlePopulasiChange.bind(this);
        this.handleSatuanPopulasiChange =
            this.handleSatuanPopulasiChange.bind(this);
        this.handleSitusWebChange = this.handleSitusWebChange.bind(this);

        this.handleFormInsertSubmit = this.handleFormInsertSubmit.bind(this);
        this.handleFormUpdateSubmit = this.handleFormUpdateSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.showAlert = this.showAlert.bind(this);
        this.closeAlert = this.closeAlert.bind(this);
    }

    componentDidMount() {
        this.fetchDesa()
            .then((res) => {
                this.setState({ ...this.state, desaData: res.data });
            })
            .catch((err) => console.error(err));
    }

    //  >>> API CALL SECTION
    fetchDesa() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/desa")
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    fetchSingleDesa(id) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/desa/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    insertDesa() {
        return new Promise((resolve, reject) => {
            console.log(this.state.modalFormData);
            axios
                .post(
                    process.env.MIX_APP_URL + "/api/desa",
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    updateDesa(id) {
        return new Promise((resolve, reject) => {
            axios
                .put(
                    process.env.MIX_APP_URL + "/api/desa/" + id,
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteDesa(id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(process.env.MIX_APP_URL + "/api/desa/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
    // <<< API CALL SECTION

    // >>> MODAL HANDLER SECTION
    handleShowInsertModal() {
        this.setState({
            ...this.state,
            showModal: true,
            modalType: "insert",
            modalFormData: {
                nama: "",
                kecamatan_id: "",
                kepala_desa: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateDesaId: null,
        });
    }

    handleShowUpdateModal(id) {
        this.fetchSingleDesa(id)
            .then((res) => {
                // update state
                const desa = res.data;
                this.setState({
                    ...this.state,
                    showModal: true,
                    modalType: "update",
                    modalFormData: {
                        nama: desa.nama ? desa.nama : "",
                        kecamatan_id: desa.kecamatan.id
                            ? desa.kecamatan.id
                            : "",
                        kepala_desa: desa.kepala_desa ? desa.kepala_desa : "",
                        luas: desa.luas ? desa.luas : "",
                        satuan_luas: desa.satuan_luas ? desa.satuan_luas : "",
                        populasi: desa.populasi ? desa.populasi : "",
                        satuan_populasi: desa.satuan_populasi
                            ? desa.satuan_populasi
                            : "",
                        situs_web: desa.situs_web ? desa.situs_web : "",
                    },
                    updateDesaId: id,
                });
            })
            .catch((err) => console.error(err));
    }

    handleShowDeleteModal(id) {
        this.setState({
            ...this.state,
            showDeleteModal: true,
            deleteDesaId: id,
        });
    }

    handleCloseModal() {
        this.setState({
            ...this.state,
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                kecamatan_id: "",
                kepala_desa: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateDesaId: null,
            deleteDesaId: null,
        });
    }
    // <<< MODAL HANDLER SECTION

    // >>> HANDLE FORM CHANGE
    handleNamaChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            nama: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleKecamatanChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            kecamatan_id: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleKepalaDesaChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            kepala_desa: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handlePopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanPopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSitusWebChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            situs_web: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    // <<< HANDLE FORM CHANGE

    // >>> HANDLE CRUD ACTION
    handleFormInsertSubmit() {
        this.insertDesa()
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Input Data Desa.");
                this.fetchDesa()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            desaData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.error(error);
                this.showAlert("danger", "Gagal Input Data Desa!");
            });
    }

    handleFormUpdateSubmit() {
        // console.log("updating", this.state.updateDesaId);
        this.updateDesa(this.state.updateDesaId)
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Update Data Desa.");
                this.fetchDesa()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            desaData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Update Data Desa!");
            });
    }

    handleDelete() {
        // console.log("deleting", this.state.deleteDesaId);
        this.deleteDesa(this.state.deleteDesaId)
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Menghapus Data Desa.");
                this.fetchDesa()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            desaData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((err) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Menghapus Data Desa!");
            });
    }
    // <<< HANDLE CRUD ACTION

    showAlert(variant, message) {
        this.setState({
            ...this.state,
            showAlert: true,
            alertVar: variant,
            alertMsg: message,
        });
    }

    closeAlert() {
        this.setState({
            ...this.state,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        });
    }

    render() {
        return (
            <Card>
                <Card.Header>Desa</Card.Header>
                <Card.Body>
                    <Card.Title>
                        <Row>
                            <Col></Col>
                            <Col className="flex-grow-0">
                                <Button
                                    className="btn-nowrap"
                                    onClick={this.handleShowInsertModal}
                                >
                                    <FaPlus /> Tambah
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>

                    <DesaTable
                        desaData={this.state.desaData}
                        handleShowUpdateModal={this.handleShowUpdateModal}
                        handleShowDeleteModal={this.handleShowDeleteModal}
                    />

                    <DesaModal
                        showModal={this.state.showModal}
                        handleCloseModal={this.handleCloseModal}
                        modalType={this.state.modalType}
                        modalFormData={this.state.modalFormData}
                        handleNamaChange={this.handleNamaChange}
                        handleKecamatanChange={this.handleKecamatanChange}
                        handleKepalaDesaChange={this.handleKepalaDesaChange}
                        handleLuasChange={this.handleLuasChange}
                        handleSatuanLuasChange={this.handleSatuanLuasChange}
                        handlePopulasiChange={this.handlePopulasiChange}
                        handleSatuanPopulasiChange={
                            this.handleSatuanPopulasiChange
                        }
                        handleSitusWebChange={this.handleSitusWebChange}
                        handleFormInsertSubmit={this.handleFormInsertSubmit}
                        handleFormUpdateSubmit={this.handleFormUpdateSubmit}
                    />

                    <DesaDeleteModal
                        showModal={this.state.showDeleteModal}
                        handleCloseModal={this.handleCloseModal}
                        handleDelete={this.handleDelete}
                    />

                    <Alert
                        show={this.state.showAlert}
                        variant={this.state.alertVar}
                        onClose={() => this.closeAlert()}
                        dismissible
                    >
                        <span>{this.state.alertMsg}</span>
                    </Alert>
                </Card.Body>
            </Card>
        );
    }
}
