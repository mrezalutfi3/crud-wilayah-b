import React, { Component } from "react";
import { Form, InputGroup } from "react-bootstrap";

export default class DesaForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kecamatanList: [],
        };
    }

    componentDidMount() {
        this.fetchKecamatan()
            .then((res) => {
                const kecamatan = res.data.map((item) => {
                    return { id: item.id, nama: item.nama };
                });
                this.setState({
                    kecamatanList: kecamatan,
                });
            })
            .catch((err) => console.err(err));
    }

    fetchKecamatan() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/kecamatan")
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    render() {
        return (
            <Form>
                <Form.Group controlId="formNama">
                    <Form.Label>Nama Desa</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.nama}
                        onChange={this.props.handleNamaChange}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="formSelectKecamatan">
                    <Form.Label>Kecamatan</Form.Label>
                    <Form.Control
                        as="select"
                        custom
                        onChange={this.props.handleKecamatanChange}
                    >
                        <option selected disabled>
                            Pilih Kecamatan
                        </option>
                        {this.state.kecamatanList.map((item) => {
                            return (
                                <option
                                    key={item.id}
                                    value={item.id}
                                    selected={
                                        item.id ==
                                        this.props.formData.kecamatan_id
                                    }
                                >
                                    {item.nama}
                                </option>
                            );
                        })}
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="formKepalaDesa">
                    <Form.Label>Nama Kepala Desa</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.kepala_desa}
                        onChange={this.props.handleKepalaDesaChange}
                    />
                </Form.Group>
                <Form.Group controlId="formLuas">
                    <Form.Label>Luas Wilayah</Form.Label>
                    <InputGroup>
                        <Form.Control
                            type="number"
                            value={this.props.formData.luas}
                            onChange={this.props.handleLuasChange}
                        />
                        <Form.Control
                            type="text"
                            value={this.props.formData.satuan_luas}
                            onChange={this.props.handleSatuanLuasChange}
                            placeholder="Satuan Luas. contoh: km^2"
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="formPopulasi">
                    <Form.Label>Populasi Penduduk</Form.Label>
                    <InputGroup>
                        <Form.Control
                            type="number"
                            value={this.props.formData.populasi}
                            onChange={this.props.handlePopulasiChange}
                        />
                        <Form.Control
                            type="text"
                            value={this.props.formData.satuan_populasi}
                            onChange={this.props.handleSatuanPopulasiChange}
                            placeholder="Satuan populasi. contoh: juta"
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="formSitusWeb">
                    <Form.Label>Situs Web</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.situs_web}
                        onChange={this.props.handleSitusWebChange}
                    />
                </Form.Group>
            </Form>
        );
    }
}
