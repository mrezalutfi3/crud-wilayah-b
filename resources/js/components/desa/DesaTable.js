import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";
import { FaEdit, FaTrash } from "react-icons/fa";

export default class DesaTable extends Component {
    render() {
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th style={{ width: "40px" }}>#</th>
                        <th>Nama</th>
                        <th>Kecamatan</th>
                        <th>Kepala Desa</th>
                        <th>Luas</th>
                        <th>Populasi</th>
                        <th>Situs Web</th>
                        <th style={{ width: "200px" }}>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.desaData.map((data, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{data.nama}</td>
                            <td>{data.kecamatan.nama}</td>
                            <td>{data.kepala_desa}</td>
                            <td>
                                {data.luas} {data.satuan_luas}
                            </td>
                            <td>
                                {data.populasi} {data.satuan_populasi}
                            </td>
                            <td>{data.situs_web}</td>
                            <td>
                                <div className="button__wrapper">
                                    <Button
                                        variant="warning"
                                        onClick={() =>
                                            this.props.handleShowUpdateModal(
                                                data.id
                                            )
                                        }
                                    >
                                        <FaEdit /> Edit
                                    </Button>
                                    <Button
                                        variant="danger"
                                        onClick={() =>
                                            this.props.handleShowDeleteModal(
                                                data.id
                                            )
                                        }
                                    >
                                        <FaTrash /> Hapus
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        );
    }
}
