import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import KabupatenForm from "./KabupatenForm";

export class KabupatenModal extends Component {
    constructor(props) {
        super(props);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
    }

    handleFormSubmit() {
        if (this.props.modalType == "insert") {
            this.props.handleCloseModal();
            this.props.handleFormInsertSubmit();
        } else if (this.props.modalType == "update") {
            this.props.handleCloseModal();
            this.props.handleFormUpdateSubmit();
        }
    }

    render() {
        return (
            <Modal
                show={this.props.showModal}
                onHide={this.props.handleCloseModal}
                backdrop="static"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        {this.props.modalType == "insert" &&
                            "Tambah Data Kabupaten/Kota"}
                        {this.props.modalType == "update" &&
                            "Update Data Kabupaten/Kota"}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <KabupatenForm
                        formType={this.props.modalType}
                        formData={this.props.modalFormData}
                        handleNamaChange={this.props.handleNamaChange}
                        handleProvinsiChange={this.props.handleProvinsiChange}
                        handleWaliKotaChange={this.props.handleWaliKotaChange}
                        handleLuasChange={this.props.handleLuasChange}
                        handleSatuanLuasChange={
                            this.props.handleSatuanLuasChange
                        }
                        handlePopulasiChange={this.props.handlePopulasiChange}
                        handleSatuanPopulasiChange={
                            this.props.handleSatuanPopulasiChange
                        }
                        handleSitusWebChange={this.props.handleSitusWebChange}
                    />
                </Modal.Body>
                <Modal.Footer className="justify-content-between">
                    <Button
                        variant="secondary"
                        onClick={this.props.handleCloseModal}
                    >
                        Tutup
                    </Button>
                    <Button variant="primary" onClick={this.handleFormSubmit}>
                        {this.props.modalType == "insert" && "Tambah Data"}
                        {this.props.modalType == "update" && "Update Data"}
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export class KabupatenDeleteModal extends Component {
    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        this.props.handleCloseModal();
        this.props.handleDelete();
    }

    render() {
        return (
            <Modal
                show={this.props.showModal}
                onHide={() => this.props.handleCloseModal()}
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5>Apakah anda yakin ingin menghapus data ini?</h5>
                </Modal.Body>
                <Modal.Footer className="justify-content-between">
                    <Button
                        variant="secondary"
                        onClick={() => this.props.handleCloseModal()}
                    >
                        Batal
                    </Button>
                    <Button
                        variant="danger"
                        onClick={() => this.handleDelete()}
                    >
                        Hapus
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
