import React, { Component } from "react";
import axios from "axios";
import { FaPlus } from "react-icons/fa";
import { Row, Col, Card, Button, Alert } from "react-bootstrap";

import { KabupatenModal, KabupatenDeleteModal } from "./KabupatenModal";
import KabupatenTable from "./KabupatenTable";

export default class Kabupaten extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kabupatenData: [],
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                provinsi_id: "",
                wali_kota: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateKabupatenId: null,
            deleteKabupatenId: null,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        };

        this.handleShowInsertModal = this.handleShowInsertModal.bind(this);
        this.handleShowUpdateModal = this.handleShowUpdateModal.bind(this);
        this.handleShowDeleteModal = this.handleShowDeleteModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        this.handleNamaChange = this.handleNamaChange.bind(this);
        this.handleProvinsiChange = this.handleProvinsiChange.bind(this);
        this.handleWaliKotaChange = this.handleWaliKotaChange.bind(this);
        this.handleLuasChange = this.handleLuasChange.bind(this);
        this.handleSatuanLuasChange = this.handleSatuanLuasChange.bind(this);
        this.handlePopulasiChange = this.handlePopulasiChange.bind(this);
        this.handleSatuanPopulasiChange =
            this.handleSatuanPopulasiChange.bind(this);
        this.handleSitusWebChange = this.handleSitusWebChange.bind(this);

        this.handleFormInsertSubmit = this.handleFormInsertSubmit.bind(this);
        this.handleFormUpdateSubmit = this.handleFormUpdateSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.showAlert = this.showAlert.bind(this);
        this.closeAlert = this.closeAlert.bind(this);
    }

    componentDidMount() {
        this.fetchKabupaten()
            .then((res) => {
                this.setState({ ...this.state, kabupatenData: res.data });
            })
            .catch((err) => console.error(err));
    }

    //  >>> API CALL SECTION
    fetchKabupaten() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/kabupaten")
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    fetchSingleKabupaten(id) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/kabupaten/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    insertKabupaten() {
        return new Promise((resolve, reject) => {
            axios
                .post(
                    process.env.MIX_APP_URL + "/api/kabupaten",
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    updateKabupaten(id) {
        return new Promise((resolve, reject) => {
            axios
                .put(
                    process.env.MIX_APP_URL + "/api/kabupaten/" + id,
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteKabupaten(id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(process.env.MIX_APP_URL + "/api/kabupaten/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
    // <<< API CALL SECTION

    // >>> MODAL HANDLER SECTION
    handleShowInsertModal() {
        this.setState({
            ...this.state,
            showModal: true,
            modalType: "insert",
            modalFormData: {
                nama: "",
                provinsi_id: "",
                wali_kota: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateKabupatenId: null,
        });
    }

    handleShowUpdateModal(id) {
        this.fetchSingleKabupaten(id)
            .then((res) => {
                // update state
                const kabupaten = res.data;
                this.setState({
                    ...this.state,
                    showModal: true,
                    modalType: "update",
                    modalFormData: {
                        nama: kabupaten.nama ? kabupaten.nama : "",
                        provinsi_id: kabupaten.provinsi.id
                            ? kabupaten.provinsi.id
                            : "",
                        wali_kota: kabupaten.wali_kota
                            ? kabupaten.wali_kota
                            : "",
                        luas: kabupaten.luas ? kabupaten.luas : "",
                        satuan_luas: kabupaten.satuan_luas
                            ? kabupaten.satuan_luas
                            : "",
                        populasi: kabupaten.populasi ? kabupaten.populasi : "",
                        satuan_populasi: kabupaten.satuan_populasi
                            ? kabupaten.satuan_populasi
                            : "",
                        situs_web: kabupaten.situs_web
                            ? kabupaten.situs_web
                            : "",
                    },
                    updateKabupatenId: id,
                });
            })
            .catch((err) => console.error(err));
    }

    handleShowDeleteModal(id) {
        this.setState({
            ...this.state,
            showDeleteModal: true,
            deleteKabupatenId: id,
        });
    }

    handleCloseModal() {
        this.setState({
            ...this.state,
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                provinsi_id: "",
                wali_kota: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateKabupatenId: null,
            deleteKabupatenId: null,
        });
    }
    // <<< MODAL HANDLER SECTION

    // >>> HANDLE FORM CHANGE
    handleNamaChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            nama: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleProvinsiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            provinsi_id: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleWaliKotaChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            wali_kota: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handlePopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanPopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSitusWebChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            situs_web: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    // <<< HANDLE FORM CHANGE

    // >>> HANDLE CRUD ACTION
    handleFormInsertSubmit() {
        this.insertKabupaten()
            .then((res) => {
                // console.log(res);
                this.showAlert(
                    "success",
                    "Berhasil Input Data Kabupaten/Kota."
                );
                this.fetchKabupaten()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            kabupatenData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.error(error);
                this.showAlert("danger", "Gagal Input Data Kabupaten/Kota!");
            });
    }

    handleFormUpdateSubmit() {
        // console.log("updating", this.state.updateKabupatenId);
        this.updateKabupaten(this.state.updateKabupatenId)
            .then((res) => {
                // console.log(res);
                this.showAlert(
                    "success",
                    "Berhasil Update Data kabupaten/Kota."
                );
                this.fetchKabupaten()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            kabupatenData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Update Data Kabupaten/Kota!");
            });
    }

    handleDelete() {
        // console.log("deleting", this.state.deleteKabupatenId);
        this.deleteKabupaten(this.state.deleteKabupatenId)
            .then((res) => {
                // console.log(res);
                this.showAlert(
                    "success",
                    "Berhasil Menghapus Data Kabupaten/Kota."
                );
                this.fetchKabupaten()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            kabupatenData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((err) => {
                // console.log(err)
                this.showAlert(
                    "danger",
                    "Gagal Menghapus Data Kabupaten/Kota!"
                );
            });
    }
    // <<< HANDLE CRUD ACTION

    showAlert(variant, message) {
        this.setState({
            ...this.state,
            showAlert: true,
            alertVar: variant,
            alertMsg: message,
        });
    }

    closeAlert() {
        this.setState({
            ...this.state,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        });
    }

    render() {
        return (
            <Card>
                <Card.Header>Kabupaten</Card.Header>
                <Card.Body>
                    <Card.Title>
                        <Row>
                            <Col></Col>
                            <Col className="flex-grow-0">
                                <Button
                                    className="btn-nowrap"
                                    onClick={this.handleShowInsertModal}
                                >
                                    <FaPlus /> Tambah
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>

                    <KabupatenTable
                        kabupatenData={this.state.kabupatenData}
                        handleShowUpdateModal={this.handleShowUpdateModal}
                        handleShowDeleteModal={this.handleShowDeleteModal}
                    />

                    <KabupatenModal
                        showModal={this.state.showModal}
                        handleCloseModal={this.handleCloseModal}
                        modalType={this.state.modalType}
                        modalFormData={this.state.modalFormData}
                        handleNamaChange={this.handleNamaChange}
                        handleProvinsiChange={this.handleProvinsiChange}
                        handleWaliKotaChange={this.handleWaliKotaChange}
                        handleLuasChange={this.handleLuasChange}
                        handleSatuanLuasChange={this.handleSatuanLuasChange}
                        handlePopulasiChange={this.handlePopulasiChange}
                        handleSatuanPopulasiChange={
                            this.handleSatuanPopulasiChange
                        }
                        handleSitusWebChange={this.handleSitusWebChange}
                        handleFormInsertSubmit={this.handleFormInsertSubmit}
                        handleFormUpdateSubmit={this.handleFormUpdateSubmit}
                    />

                    <KabupatenDeleteModal
                        showModal={this.state.showDeleteModal}
                        handleCloseModal={this.handleCloseModal}
                        handleDelete={this.handleDelete}
                    />

                    <Alert
                        show={this.state.showAlert}
                        variant={this.state.alertVar}
                        onClose={() => this.closeAlert()}
                        dismissible
                    >
                        <span>{this.state.alertMsg}</span>
                    </Alert>
                </Card.Body>
            </Card>
        );
    }
}
