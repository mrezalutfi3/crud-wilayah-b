import React, { Component } from "react";
import { Form, InputGroup } from "react-bootstrap";

export default class KecamatanForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kabupatenList: [],
        };
    }

    componentDidMount() {
        this.fetchKabupaten()
            .then((res) => {
                const kabupaten = res.data.map((item) => {
                    return { id: item.id, nama: item.nama };
                });
                this.setState({
                    kabupatenList: kabupaten,
                });
            })
            .catch((err) => console.err(err));
    }

    fetchKabupaten() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/kabupaten")
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    render() {
        return (
            <Form>
                <Form.Group controlId="formNama">
                    <Form.Label>Nama Kecamatan</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.nama}
                        onChange={this.props.handleNamaChange}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="formSelectKabupaten">
                    <Form.Label>Kabupaten</Form.Label>
                    <Form.Control
                        as="select"
                        custom
                        onChange={this.props.handleKabupatenChange}
                    >
                        <option selected disabled>
                            Pilih Kabupaten
                        </option>
                        {this.state.kabupatenList.map((item) => {
                            return (
                                <option
                                    key={item.id}
                                    value={item.id}
                                    selected={
                                        item.id ==
                                        this.props.formData.kabupaten_kota_id
                                    }
                                >
                                    {item.nama}
                                </option>
                            );
                        })}
                    </Form.Control>
                </Form.Group>
                <Form.Group controlId="formCamat">
                    <Form.Label>Nama Camat</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.camat}
                        onChange={this.props.handleCamatChange}
                    />
                </Form.Group>
                <Form.Group controlId="formLuas">
                    <Form.Label>Luas Wilayah</Form.Label>
                    <InputGroup>
                        <Form.Control
                            type="number"
                            value={this.props.formData.luas}
                            onChange={this.props.handleLuasChange}
                        />
                        <Form.Control
                            type="text"
                            value={this.props.formData.satuan_luas}
                            onChange={this.props.handleSatuanLuasChange}
                            placeholder="Satuan Luas. contoh: km^2"
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="formPopulasi">
                    <Form.Label>Populasi Penduduk</Form.Label>
                    <InputGroup>
                        <Form.Control
                            type="number"
                            value={this.props.formData.populasi}
                            onChange={this.props.handlePopulasiChange}
                        />
                        <Form.Control
                            type="text"
                            value={this.props.formData.satuan_populasi}
                            onChange={this.props.handleSatuanPopulasiChange}
                            placeholder="Satuan populasi. contoh: juta"
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="formSitusWeb">
                    <Form.Label>Situs Web</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.situs_web}
                        onChange={this.props.handleSitusWebChange}
                    />
                </Form.Group>
            </Form>
        );
    }
}
