import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";
import { FaEdit, FaTrash } from "react-icons/fa";

export default class KecamatanTable extends Component {
    render() {
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th style={{ width: "40px" }}>#</th>
                        <th>Nama</th>
                        <th>Kabupaten / Kota</th>
                        <th>Camat</th>
                        <th>Luas</th>
                        <th>Populasi</th>
                        <th>Jumlah Desa</th>
                        <th>Situs Web</th>
                        <th style={{ width: "200px" }}>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.kecamatanData.map((data, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{data.nama}</td>
                            <td>{data.kabupaten.nama}</td>
                            <td>{data.camat}</td>
                            <td>
                                {data.luas} {data.satuan_luas}
                            </td>
                            <td>
                                {data.populasi} {data.satuan_populasi}
                            </td>
                            <td>{data.jumlah_desa}</td>
                            <td>{data.situs_web}</td>
                            <td>
                                <div className="button__wrapper">
                                    <Button
                                        variant="warning"
                                        onClick={() =>
                                            this.props.handleShowUpdateModal(
                                                data.id
                                            )
                                        }
                                    >
                                        <FaEdit /> Edit
                                    </Button>
                                    <Button
                                        variant="danger"
                                        onClick={() =>
                                            this.props.handleShowDeleteModal(
                                                data.id
                                            )
                                        }
                                    >
                                        <FaTrash /> Hapus
                                    </Button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        );
    }
}
