import React, { Component } from "react";
import axios from "axios";
import { FaPlus } from "react-icons/fa";
import { Row, Col, Card, Button, Alert } from "react-bootstrap";

import { KecamatanModal, KecamatanDeleteModal } from "./KecamatanModal";
import KecamatanTable from "./KecamatanTable";

export default class Kecamatan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            kecamatanData: [],
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                kabupaten_kota_id: "",
                camat: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateKecamatanId: null,
            deleteKecamatanId: null,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        };

        this.handleShowInsertModal = this.handleShowInsertModal.bind(this);
        this.handleShowUpdateModal = this.handleShowUpdateModal.bind(this);
        this.handleShowDeleteModal = this.handleShowDeleteModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        this.handleNamaChange = this.handleNamaChange.bind(this);
        this.handleKabupatenChange = this.handleKabupatenChange.bind(this);
        this.handleCamatChange = this.handleCamatChange.bind(this);
        this.handleLuasChange = this.handleLuasChange.bind(this);
        this.handleSatuanLuasChange = this.handleSatuanLuasChange.bind(this);
        this.handlePopulasiChange = this.handlePopulasiChange.bind(this);
        this.handleSatuanPopulasiChange =
            this.handleSatuanPopulasiChange.bind(this);
        this.handleSitusWebChange = this.handleSitusWebChange.bind(this);

        this.handleFormInsertSubmit = this.handleFormInsertSubmit.bind(this);
        this.handleFormUpdateSubmit = this.handleFormUpdateSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.showAlert = this.showAlert.bind(this);
        this.closeAlert = this.closeAlert.bind(this);
    }

    componentDidMount() {
        this.fetchKecamatan()
            .then((res) => {
                this.setState({ ...this.state, kecamatanData: res.data });
            })
            .catch((err) => console.error(err));
    }

    //  >>> API CALL SECTION
    fetchKecamatan() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/kecamatan")
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    fetchSingleKecamatan(id) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/kecamatan/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    insertKecamatan() {
        return new Promise((resolve, reject) => {
            axios
                .post(
                    process.env.MIX_APP_URL + "/api/kecamatan",
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    updateKecamatan(id) {
        return new Promise((resolve, reject) => {
            axios
                .put(
                    process.env.MIX_APP_URL + "/api/kecamatan/" + id,
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteKecamatan(id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(process.env.MIX_APP_URL + "/api/kecamatan/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
    // <<< API CALL SECTION

    // >>> MODAL HANDLER SECTION
    handleShowInsertModal() {
        this.setState({
            ...this.state,
            showModal: true,
            modalType: "insert",
            modalFormData: {
                nama: "",
                kabupaten_kota_id: "",
                camat: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateKecamatanId: null,
        });
    }

    handleShowUpdateModal(id) {
        this.fetchSingleKecamatan(id)
            .then((res) => {
                // update state
                const kecamatan = res.data;
                this.setState({
                    ...this.state,
                    showModal: true,
                    modalType: "update",
                    modalFormData: {
                        nama: kecamatan.nama ? kecamatan.nama : "",
                        kabupaten_kota_id: kecamatan.kabupaten.id
                            ? kecamatan.kabupaten.id
                            : "",
                        camat: kecamatan.camat ? kecamatan.camat : "",
                        luas: kecamatan.luas ? kecamatan.luas : "",
                        satuan_luas: kecamatan.satuan_luas
                            ? kecamatan.satuan_luas
                            : "",
                        populasi: kecamatan.populasi ? kecamatan.populasi : "",
                        satuan_populasi: kecamatan.satuan_populasi
                            ? kecamatan.satuan_populasi
                            : "",
                        situs_web: kecamatan.situs_web
                            ? kecamatan.situs_web
                            : "",
                    },
                    updateKecamatanId: id,
                });
            })
            .catch((err) => console.error(err));
    }

    handleShowDeleteModal(id) {
        this.setState({
            ...this.state,
            showDeleteModal: true,
            deleteKecamatanId: id,
        });
    }

    handleCloseModal() {
        this.setState({
            ...this.state,
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                kabupaten_kota_id: "",
                camat: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateKecamatanId: null,
            deleteKecamatanId: null,
        });
    }
    // <<< MODAL HANDLER SECTION

    // >>> HANDLE FORM CHANGE
    handleNamaChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            nama: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleKabupatenChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            kabupaten_kota_id: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleCamatChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            camat: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handlePopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanPopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSitusWebChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            situs_web: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    // <<< HANDLE FORM CHANGE

    // >>> HANDLE CRUD ACTION
    handleFormInsertSubmit() {
        this.insertKecamatan()
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Input Data Kecamatan.");
                this.fetchKecamatan()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            kecamatanData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.error(error);
                this.showAlert("danger", "Gagal Input Data Kecamatan!");
            });
    }

    handleFormUpdateSubmit() {
        // console.log("updating", this.state.updateKecamatanId);
        this.updateKecamatan(this.state.updateKecamatanId)
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Update Data kecamatan.");
                this.fetchKecamatan()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            kecamatanData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Update Data Kecamatan!");
            });
    }

    handleDelete() {
        // console.log("deleting", this.state.deleteKecamatanId);
        this.deleteKecamatan(this.state.deleteKecamatanId)
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Menghapus Data Kecamatan.");
                this.fetchKecamatan()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            kecamatanData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((err) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Menghapus Data Kecamatan!");
            });
    }
    // <<< HANDLE CRUD ACTION

    showAlert(variant, message) {
        this.setState({
            ...this.state,
            showAlert: true,
            alertVar: variant,
            alertMsg: message,
        });
    }

    closeAlert() {
        this.setState({
            ...this.state,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        });
    }

    render() {
        return (
            <Card>
                <Card.Header>Kecamatan</Card.Header>
                <Card.Body>
                    <Card.Title>
                        <Row>
                            <Col></Col>
                            <Col className="flex-grow-0">
                                <Button
                                    className="btn-nowrap"
                                    onClick={this.handleShowInsertModal}
                                >
                                    <FaPlus /> Tambah
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>

                    <KecamatanTable
                        kecamatanData={this.state.kecamatanData}
                        handleShowUpdateModal={this.handleShowUpdateModal}
                        handleShowDeleteModal={this.handleShowDeleteModal}
                    />

                    <KecamatanModal
                        showModal={this.state.showModal}
                        handleCloseModal={this.handleCloseModal}
                        modalType={this.state.modalType}
                        modalFormData={this.state.modalFormData}
                        handleNamaChange={this.handleNamaChange}
                        handleKabupatenChange={this.handleKabupatenChange}
                        handleCamatChange={this.handleCamatChange}
                        handleLuasChange={this.handleLuasChange}
                        handleSatuanLuasChange={this.handleSatuanLuasChange}
                        handlePopulasiChange={this.handlePopulasiChange}
                        handleSatuanPopulasiChange={
                            this.handleSatuanPopulasiChange
                        }
                        handleSitusWebChange={this.handleSitusWebChange}
                        handleFormInsertSubmit={this.handleFormInsertSubmit}
                        handleFormUpdateSubmit={this.handleFormUpdateSubmit}
                    />

                    <KecamatanDeleteModal
                        showModal={this.state.showDeleteModal}
                        handleCloseModal={this.handleCloseModal}
                        handleDelete={this.handleDelete}
                    />

                    <Alert
                        show={this.state.showAlert}
                        variant={this.state.alertVar}
                        onClose={() => this.closeAlert()}
                        dismissible
                    >
                        <span>{this.state.alertMsg}</span>
                    </Alert>
                </Card.Body>
            </Card>
        );
    }
}
