import React, { Component } from "react";
import axios from "axios";
import { Form, InputGroup } from "react-bootstrap";

export default class ProvinsiForm extends Component {
    render() {
        return (
            <Form>
                <Form.Group controlId="formNama">
                    <Form.Label>Nama Provinsi</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.nama}
                        onChange={this.props.handleNamaChange}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="formGubernur">
                    <Form.Label>Nama Gubernur</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.gubernur}
                        onChange={this.props.handleGubernurChange}
                    />
                </Form.Group>
                <Form.Group controlId="formLuas">
                    <Form.Label>Luas Wilayah</Form.Label>
                    <InputGroup>
                        <Form.Control
                            type="number"
                            value={this.props.formData.luas}
                            onChange={this.props.handleLuasChange}
                        />
                        <Form.Control
                            type="text"
                            value={this.props.formData.satuan_luas}
                            onChange={this.props.handleSatuanLuasChange}
                            placeholder="Satuan Luas. contoh: km^2"
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="formPopulasi">
                    <Form.Label>Populasi Penduduk</Form.Label>
                    <InputGroup>
                        <Form.Control
                            type="number"
                            value={this.props.formData.populasi}
                            onChange={this.props.handlePopulasiChange}
                        />
                        <Form.Control
                            type="text"
                            value={this.props.formData.satuan_populasi}
                            onChange={this.props.handleSatuanPopulasiChange}
                            placeholder="Satuan populasi. contoh: juta"
                        />
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="formSitusWeb">
                    <Form.Label>Situs Web</Form.Label>
                    <Form.Control
                        type="text"
                        value={this.props.formData.situs_web}
                        onChange={this.props.handleSitusWebChange}
                    />
                </Form.Group>
            </Form>
        );
    }
}
