import React, { Component } from "react";
import axios from "axios";
import { FaPlus } from "react-icons/fa";
import { Row, Col, Card, Button, Alert } from "react-bootstrap";

import { ProvinsiModal, ProvinsiDeleteModal } from "./ProvinsiModal";
import ProvinsiTable from "./ProvinsiTable";

export default class Provinsi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            provinsiData: [],
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                gubernur: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateProvinsiId: null,
            deleteProvinsiId: null,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        };

        this.handleShowInsertModal = this.handleShowInsertModal.bind(this);
        this.handleShowUpdateModal = this.handleShowUpdateModal.bind(this);
        this.handleShowDeleteModal = this.handleShowDeleteModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);

        this.handleNamaChange = this.handleNamaChange.bind(this);
        this.handleGubernurChange = this.handleGubernurChange.bind(this);
        this.handleLuasChange = this.handleLuasChange.bind(this);
        this.handleSatuanLuasChange = this.handleSatuanLuasChange.bind(this);
        this.handlePopulasiChange = this.handlePopulasiChange.bind(this);
        this.handleSatuanPopulasiChange =
            this.handleSatuanPopulasiChange.bind(this);
        this.handleSitusWebChange = this.handleSitusWebChange.bind(this);

        this.handleFormInsertSubmit = this.handleFormInsertSubmit.bind(this);
        this.handleFormUpdateSubmit = this.handleFormUpdateSubmit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

        this.showAlert = this.showAlert.bind(this);
        this.closeAlert = this.closeAlert.bind(this);
    }

    componentDidMount() {
        this.fetchProvinsi()
            .then((res) => {
                this.setState({ ...this.state, provinsiData: res.data });
            })
            .catch((err) => console.error(err));
    }

    //  >>> API CALL SECTION
    fetchProvinsi() {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/provinsi")
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    fetchSingleProvinsi(id) {
        return new Promise((resolve, reject) => {
            axios
                .get(process.env.MIX_APP_URL + "/api/provinsi/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    insertProvinsi() {
        return new Promise((resolve, reject) => {
            axios
                .post(
                    process.env.MIX_APP_URL + "/api/provinsi",
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    updateProvinsi(id) {
        return new Promise((resolve, reject) => {
            axios
                .put(
                    process.env.MIX_APP_URL + "/api/provinsi/" + id,
                    this.state.modalFormData
                )
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }

    deleteProvinsi(id) {
        return new Promise((resolve, reject) => {
            axios
                .delete(process.env.MIX_APP_URL + "/api/provinsi/" + id)
                .then((res) => {
                    resolve(res.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    }
    // <<< API CALL SECTION

    // >>> MODAL HANDLER SECTION
    handleShowInsertModal() {
        this.setState({
            ...this.state,
            showModal: true,
            modalType: "insert",
            modalFormData: {
                nama: "",
                gubernur: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateProvinsiId: null,
        });
    }

    handleShowUpdateModal(id) {
        this.fetchSingleProvinsi(id)
            .then((res) => {
                // update state
                const provinsi = res.data;
                this.setState({
                    ...this.state,
                    showModal: true,
                    modalType: "update",
                    modalFormData: {
                        nama: provinsi.nama ? provinsi.nama : "",
                        gubernur: provinsi.gubernur ? provinsi.gubernur : "",
                        luas: provinsi.luas ? provinsi.luas : "",
                        satuan_luas: provinsi.satuan_luas
                            ? provinsi.satuan_luas
                            : "",
                        populasi: provinsi.populasi ? provinsi.populasi : "",
                        satuan_populasi: provinsi.satuan_populasi
                            ? provinsi.satuan_populasi
                            : "",
                        situs_web: provinsi.situs_web ? provinsi.situs_web : "",
                    },
                    updateProvinsiId: id,
                });
            })
            .catch((err) => console.error(err));
    }

    handleShowDeleteModal(id) {
        this.setState({
            ...this.state,
            showDeleteModal: true,
            deleteProvinsiId: id,
        });
    }

    handleCloseModal() {
        this.setState({
            ...this.state,
            showModal: false,
            showDeleteModal: false,
            modalType: null,
            modalFormData: {
                nama: "",
                gubernur: "",
                luas: "",
                satuan_luas: "",
                populasi: "",
                satuan_populasi: "",
                situs_web: "",
            },
            updateProvinsiId: null,
            deleteProvinsiId: null,
        });
    }
    // <<< MODAL HANDLER SECTION

    // >>> HANDLE FORM CHANGE
    handleNamaChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            nama: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleGubernurChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            gubernur: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanLuasChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_luas: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handlePopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSatuanPopulasiChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            satuan_populasi: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    handleSitusWebChange(e) {
        const newFormData = {
            ...this.state.modalFormData,
            situs_web: e.target.value,
        };
        this.setState({
            ...this.state,
            modalFormData: newFormData,
        });
    }
    // <<< HANDLE FORM CHANGE

    // >>> HANDLE CRUD ACTION
    handleFormInsertSubmit() {
        this.insertProvinsi()
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Input Data Provinsi.");
                this.fetchProvinsi()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            provinsiData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((error) => {
                // console.error(error);
                this.showAlert("danger", "Gagal Input Data Provinsi!");
            });
    }

    handleFormUpdateSubmit() {
        // console.log("updating", this.state.updateProvinsiId);
        this.updateProvinsi(this.state.updateProvinsiId)
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Update Data Provinsi.");
                this.fetchProvinsi()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            provinsiData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((err) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Update Data Provinsi!");
            });
    }

    handleDelete() {
        // console.log("deleting", this.state.deleteProvinsiId);
        this.deleteProvinsi(this.state.deleteProvinsiId)
            .then((res) => {
                // console.log(res);
                this.showAlert("success", "Berhasil Menghapus Data Provinsi.");
                this.fetchProvinsi()
                    .then((res) => {
                        this.setState({
                            ...this.state,
                            provinsiData: res.data,
                        });
                    })
                    .catch((err) => console.error(err));
            })
            .catch((err) => {
                // console.log(err)
                this.showAlert("danger", "Gagal Menghapus Data Provinsi!");
            });
    }
    // <<< HANDLE CRUD ACTION

    showAlert(variant, message) {
        this.setState({
            ...this.state,
            showAlert: true,
            alertVar: variant,
            alertMsg: message,
        });
    }

    closeAlert() {
        this.setState({
            ...this.state,
            showAlert: false,
            alertVar: "",
            alertMsg: "",
        });
    }

    render() {
        return (
            <Card>
                <Card.Header>Provinsi</Card.Header>
                <Card.Body>
                    <Card.Title>
                        <Row>
                            <Col></Col>
                            <Col className="flex-grow-0">
                                <Button
                                    className="btn-nowrap"
                                    onClick={this.handleShowInsertModal}
                                >
                                    <FaPlus /> Tambah
                                </Button>
                            </Col>
                        </Row>
                    </Card.Title>

                    <ProvinsiTable
                        provinsiData={this.state.provinsiData}
                        handleShowUpdateModal={this.handleShowUpdateModal}
                        handleShowDeleteModal={this.handleShowDeleteModal}
                    />

                    <ProvinsiModal
                        showModal={this.state.showModal}
                        handleCloseModal={this.handleCloseModal}
                        modalType={this.state.modalType}
                        modalFormData={this.state.modalFormData}
                        handleNamaChange={this.handleNamaChange}
                        handleGubernurChange={this.handleGubernurChange}
                        handleLuasChange={this.handleLuasChange}
                        handleSatuanLuasChange={this.handleSatuanLuasChange}
                        handlePopulasiChange={this.handlePopulasiChange}
                        handleSatuanPopulasiChange={
                            this.handleSatuanPopulasiChange
                        }
                        handleSitusWebChange={this.handleSitusWebChange}
                        handleFormInsertSubmit={this.handleFormInsertSubmit}
                        handleFormUpdateSubmit={this.handleFormUpdateSubmit}
                    />

                    <ProvinsiDeleteModal
                        showModal={this.state.showDeleteModal}
                        handleCloseModal={this.handleCloseModal}
                        handleDelete={this.handleDelete}
                    />

                    <Alert
                        show={this.state.showAlert}
                        variant={this.state.alertVar}
                        onClose={() => this.closeAlert()}
                        dismissible
                    >
                        <span>{this.state.alertMsg}</span>
                    </Alert>
                </Card.Body>
            </Card>
        );
    }
}
