import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import { Container, Row, Col, Navbar, Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import Provinsi from "./components/provinsi/Provinsi";
import Kabupaten from "./components/kabupaten_kota/Kabupaten";
import Kecamatan from "./components/kecamatan/Kecamatan";
import Desa from "./components/desa/Desa";

export default class App extends Component {
    render() {
        return (
            <>
                <Router>
                    <Navbar bg="dark" variant="dark">
                        <Container>
                            <LinkContainer to="/">
                                <Navbar.Brand>CRUD Wilayah</Navbar.Brand>
                            </LinkContainer>
                            <Nav className="me-auto">
                                <LinkContainer to="/provinsi">
                                    <Nav.Link>Provinsi</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/kabupaten_kota">
                                    <Nav.Link>Kabupaten/Kota</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/kecamatan">
                                    <Nav.Link>Kecamatan</Nav.Link>
                                </LinkContainer>
                                <LinkContainer to="/desa">
                                    <Nav.Link>Desa</Nav.Link>
                                </LinkContainer>
                            </Nav>
                        </Container>
                    </Navbar>
                    <Container style={{ padding: "1rem" }}>
                        <Row>
                            <Col>
                                <Switch>
                                    <Route exact path="/">
                                        <h3>Selamat Datang</h3>
                                    </Route>
                                    <Route path="/provinsi">
                                        <Provinsi />
                                    </Route>
                                    <Route path="/kabupaten_kota">
                                        <Kabupaten />
                                    </Route>
                                    <Route path="/kecamatan">
                                        <Kecamatan />
                                    </Route>
                                    <Route path="/desa">
                                        <Desa />
                                    </Route>
                                </Switch>
                            </Col>
                        </Row>
                    </Container>
                </Router>
            </>
        );
    }
}

if (document.getElementById("app")) {
    ReactDOM.render(<App />, document.getElementById("app"));
}
