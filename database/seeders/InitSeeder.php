<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinsi')
            ->insert([
                'nama' => 'Lampung'
            ]);
        DB::table('kabupaten_kota')
            ->insert([
                'nama' => 'Lampung barat',
                'provinsi_id' => 1
            ]);
            
        DB::table('kecamatan')
            ->insert([
                'nama' => 'Batu Brak',
                'kabupaten_kota_id' => 1
            ]);
                
        DB::table('desa')
            ->insert([
                'nama' => 'Kembahang',
                'kecamatan_id' => 1
            ]);


    }
}
