<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('desa', function (Blueprint $table) {
            $table->foreign('kecamatan_id')
                ->references('id')->on('kecamatan')
                ->onDelete('cascade');
        });

        Schema::table('kecamatan', function (Blueprint $table) {
            $table->foreign('kabupaten_kota_id')
                ->references('id')->on('kabupaten_kota')
                ->onDelete('cascade');
        });

        Schema::table('kabupaten_kota', function (Blueprint $table) {
            $table->foreign('provinsi_id')
                ->references('id')->on('provinsi')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
