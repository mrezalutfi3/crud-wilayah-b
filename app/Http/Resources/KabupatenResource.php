<?php

namespace App\Http\Resources;

use App\Models\Provinsi;
use App\Models\Kecamatan;
use Illuminate\Http\Resources\Json\JsonResource;

class KabupatenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'wali_kota' => $this->wali_kota,
            'luas' => $this->luas,
            'satuan_luas' => $this->satuan_luas,
            'populasi' => $this->populasi,
            'satuan_populasi' => $this->satuan_populasi,
            'jumlah_kecamatan' => count(Kecamatan::where('kabupaten_kota_id', $this->id)->get()),
            'situs_web' => $this->situs_web,
            'provinsi' => Provinsi::find($this->provinsi_id)
        ];
    }
}
