<?php

namespace App\Http\Resources;

use App\Models\Kabupaten;
use Illuminate\Http\Resources\Json\JsonResource;

class ProvinsiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'gubernur' => $this->gubernur,
            'luas' => $this->luas,
            'satuan_luas' => $this->satuan_luas,
            'populasi' => $this->populasi,
            'satuan_populasi' => $this->satuan_populasi,
            'jumlah_kab_kota' => count(Kabupaten::where('provinsi_id', $this->id)->get()),
            'situs_web' => $this->situs_web
        ];
    }
}
