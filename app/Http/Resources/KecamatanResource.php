<?php

namespace App\Http\Resources;

use App\Models\Kabupaten;
use App\Models\Desa;
use Illuminate\Http\Resources\Json\JsonResource;

class KecamatanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'camat' => $this->camat,
            'luas' => $this->luas,
            'satuan_luas' => $this->satuan_luas,
            'populasi' => $this->populasi,
            'satuan_populasi' => $this->satuan_populasi,
            'jumlah_desa' => count(Desa::where('kecamatan_id', $this->id)->get()),
            'situs_web' => $this->situs_web,
            'kabupaten' => Kabupaten::find($this->kabupaten_kota_id)
        ];
    }
}
