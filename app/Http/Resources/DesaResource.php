<?php

namespace App\Http\Resources;

use App\Models\Kecamatan;
use Illuminate\Http\Resources\Json\JsonResource;

class DesaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama' => $this->nama,
            'kepala_desa' => $this->kepala_desa,
            'luas' => $this->luas,
            'satuan_luas' => $this->satuan_luas,
            'populasi' => $this->populasi,
            'satuan_populasi' => $this->satuan_populasi,
            'situs_web' => $this->situs_web,
            'kecamatan' => Kecamatan::find($this->kecamatan_id)
        ];
    }
}
