<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProvinsiResource;
use App\Models\Provinsi;
use Illuminate\Http\Request;

class ProvinsiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ProvinsiResource::collection(Provinsi::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return Provinsi::create($request->input());
    }

    /**
     * Display the specified resource.
     */
    public function show(Provinsi $provinsi)
    {
        return new ProvinsiResource($provinsi);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        return Provinsi::where('id', $id)
                        ->update($request->input());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        return Provinsi::find($id)->delete();
    }
}
