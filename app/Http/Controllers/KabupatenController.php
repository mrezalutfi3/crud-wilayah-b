<?php

namespace App\Http\Controllers;

use App\Http\Resources\KabupatenResource;
use App\Models\Kabupaten;
use Illuminate\Http\Request;

class KabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return KabupatenResource::collection(Kabupaten::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function insert(Request $request)
    {
        return Kabupaten::create($request->input());
    }

    /**
     * Display the specified resource.
     */
    public function read($id)
    {
        return new KabupatenResource($kabupaten);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        return Kabupaten::where('id', $id)
            ->update($request->input());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        return Kabupaten::find($id)->delete();
    }
}
