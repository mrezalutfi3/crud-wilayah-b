<?php

namespace App\Http\Controllers;

use App\Http\Resources\KecamatanResource;
use App\Models\Kecamatan;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return KecamatanResource::collection(Kecamatan::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function insert(Request $request)
    {
        return Kecamatan::create($request->input());
    }

    /**
     * Display the specified resource.
     */
    public function read($id)
    {
        return new KecamatanResource($kecamatan);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        return Kecamatan::where('id', $id)
            ->update($request->input());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        return Kecamatan::find($id)->delete();
    }
}
