<?php

namespace App\Http\Controllers;

use App\Http\Resources\DesaResource;
use App\Models\Desa;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return DesaResource::collection(Desa::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function insert(Request $request)
    {
        return Desa::create($request->input());
    }

    /**
     * Display the specified resource.
     */
    public function read($id)
    {
        return new DesaResource($desa);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        return Desa::where('id', $id)
            ->update($request->input());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        return Desa::find($id)->delete();
    }
}
