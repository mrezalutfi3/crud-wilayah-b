<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    use HasFactory;

    protected $table = "desa";
    protected $fillable =
        [
            'nama',
            'kecamatan_id',
            'kepala_desa',
            'luas',
            'satuan_luas',
            'populasi',
            'satuan_populasi',
            'situs_web'
        ];

    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class);
    }
}
