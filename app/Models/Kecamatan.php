<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;

    protected $table = "kecamatan";
    protected $fillable =
        [
            'nama',
            'kabupaten_kota_id',
            'camat',
            'luas',
            'satuan_luas',
            'populasi',
            'satuan_populasi',
            'situs_web'
        ];

    public function desa()
    {
        return $this->hasMany(Desa::class);
    }

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'kabupaten_kota_id');
    }
}
