<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    use HasFactory;

    protected $table = "provinsi";
    protected $fillable =
        [
            'nama',
            'ibu_kota_id',
            'gubernur',
            'luas',
            'satuan_luas',
            'populasi',
            'satuan_populasi',
            'situs_web'
        ];

    public function kabupaten()
    {
        return $this->hasMany(Kabupaten::class);
    }

    public function ibu_kota()
    {
        return $this->hasOne(Kabupaten::class, 'ibu_kota_id');
    }
}
