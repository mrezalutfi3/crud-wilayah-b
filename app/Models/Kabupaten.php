<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    use HasFactory;

    protected $table = "kabupaten_kota";
    protected $fillable =
        [
            'nama',
            'provinsi_id',
            'ibu_kota_id',
            'wali_kota',
            'luas',
            'satuan_luas',
            'populasi',
            'satuan_populasi',
            'situs_web'
        ];

    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class);
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class);
    }

    public function ibu_kota()
    {
        return $this->hasOne(Kabupaten::class, 'ibu_kota_id');
    }
}
